function divtext(basedom) {
	travel(basedom, null, _divtext);
}

function _divtext(textNode) {
	travel_replace(textNode, _replace_divtext);
}

function _replace_divtext(text) {
	var i, temp = "";
	for (i = 0 ; i < text.length ; i++) {
		if (i != 0 && i % 20 == 0) {
			temp += "<wbr></wbr>";
		}
		temp += text.charAt(i).replace("<", "&lt").replace(">", "&gt");
	}
	return temp;
}
