# divtext
공백없이 길어서 정해진 넓이보다 길어지는 텍스트가 있다. Dom을 탐색하여 이 텍스트를 적절히 나누어 줄바꿈이 되도록 한다. ([travel.js](https://bitbucket.org/joostory/travel.js) 사용)

# 사용법
	divtext(dom);

# 사용예
	<!doctype html>
	<html>
	<head>
		<meta charset="utf-8">
		<title>Div text</title>
	</head>
	<body>
		<h1>Div text</h1>

		This is a link to Google <a href='http://google.com' target='_blank' rel='nofollow' id='1'>http://google.com</a>
		<p style="background: url(http://example.com/logo.png) ">This is a link to image http://example.com/logo.png</p>

		<script src="travel.js"></script>
		<script src="divtext.js"></script>
		<script>
			divtext(document.body);
		</script>
	</body>
	</html>
